import scrapy


class CrawlerItem(scrapy.Item):
    # define the fields for your item here like:
    # link = scrapy.Field()
    # sport = scrapy.Field()
    # name = scrapy.Field()
    # country = scrapy.Field()
    # dob = scrapy.Field()
    # gender = scrapy.Field()
    # weight = scrapy.Field()
    # height = scrapy.Field()
    # sport = scrapy.Field()

    title = scrapy.Field()
    description = scrapy.Field()
    content = scrapy.Field()