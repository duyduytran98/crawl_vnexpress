from scrapy import Spider
from scrapy.selector import Selector
import scrapy
import sys
sys.path.append('/home/duy/code/tim kiem trinh dien/crawl/crawl')
from items import CrawlerItem
import datetime
# import mysql.connector
# import uuid
# import random
# import string
# clb = ['tottenham hotspur','u21 việt nam','real madrid','real','barca','barcelona','atletico madrid','atlético madrid','mu','manchester united','mc','manchester city','chelsea','liverpool','arsenal','paris saint germain','psg','juventus','inter','inter milan','ac milan','bayern munich','bayern','borussia dortmund','dortmund','việt nam']
#
# mydb = mysql.connector.connect(
#     host="139.162.45.23",
#     user="root",
#     passwd="duydq",
#     database="chatbot_olympic"
# )
# mycursor = mydb.cursor()
data = {
    "BRUNEI":1,
    "CAMBODIA":2,
    "INDONESIA":3,
    "LAOS":4,
    "MALAYSIA":5,
    "MYANMAR":6,
    "PHILIPPINES":7,
    "SINGAPORE":8,
    "THAILAND":9,
    "TIMOR-LESTE":10,
    "VIETNAM":11
}


def get_key(val):
    for key, value in data.items():
        if val == value:
            return key

    return "key doesn't exist"
class CrawlerSpider(Spider):
    name = "vdv"
    def start_requests(self):
        urls = []
        for i in range(1,12):
            url = "https://rs.2019seagames.com/RS2019/MobiApp/Athletes?cid={}&sid=None&gender=A".format(i)
            urls.append(url)
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)
    def parse(self, response):
        athletes = Selector(response).xpath('/html/body/div/div')
        for athlete in athletes:
            href = athlete.xpath('div[1]/div/div/div/div[2]/h4/a/@href').extract_first()
            sport = athlete.xpath('div[1]/div/div/div/div[2]/small/text()[2]').extract_first()
            if href and sport:
                link = 'https://rs.2019seagames.com/'+href
                print(link,sport.strip())
                item = CrawlerItem()
                item['link']=link
                item['sport']=sport.strip()
                yield item

