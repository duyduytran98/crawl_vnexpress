from scrapy import Spider
from scrapy.selector import Selector
import scrapy
import sys
sys.path.append('/home/duy/code/tim kiem trinh dien/crawl/crawl')
from items import CrawlerItem
import json
import datetime
dm = 'doisong'

def ghep(a):
    b = []
    for i in a:
        if i.strip()!='':
            b.append(i.strip())
    x = ' '.join(b)
    return x
class CrawlerSpider(Spider):
    name = "detail"
    def start_requests(self):
        file_name = '/home/duy/code/tim kiem trinh dien/crawl/link/{}.txt'.format(dm)
        file = open(file_name,'r',encoding='utf-8-sig')
        urls = []
        for line in file:
            urls.append(line.strip())
        file.close()
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)
    def parse(self, response):
        file_name_json = '/home/duy/code/tim kiem trinh dien/crawl/data/{}.json'.format(dm)
        filejson = open(file_name_json,'a',encoding='utf-8-sig')
        title_ = response.xpath('/html/body/section[2]/section[1]/section[1]/h1/text()').extract()
        des_ = response.xpath('/html/body/section[2]/section[1]/section[1]/p/text()').extract()
        content_ = response.xpath('/html/body/section[2]/section[1]/section[1]/article//text()').extract()
        title = ghep(title_)
        decription = ghep(des_)
        content = ghep(content_)
        print(title)
        print('.................')
        print(decription)
        print('.................')
        print(content)
        print('________________________________________________________________________')
        if title!='' and decription!= '' and content!= '':
            data_ = {
                "category":dm,
                "title":title,
                "description":decription,
                "content":content
            }
            data1 = json.dumps((data_))
            filejson.write(data1)
            filejson.write('\n')


#SỬA ĐƯỜNG DẪN TRÊN MÁY MÌNH line 5,21,30
# CÀI SCRAPY: pip install scrapy
# Thay biến dm ở dòng 9 thành thể loại cần crawl
# terminal: "scrapy crawl detail"
# Duy: doisong, kinhdoanh,thethao
# Giap beo: dulich,phapluat,thoisu
# Duong: giaitri,sohoa
# Hiếu: giaoduc,suckhoe
# Phú: khoahoc,thegioi