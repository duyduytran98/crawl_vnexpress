from scrapy import Spider
from scrapy.selector import Selector
import scrapy
import sys
sys.path.append('/home/duy/code/tim kiem trinh dien/crawl/crawl')
from items import CrawlerItem
import json
import datetime

class CrawlerSpider(Spider):
    name = "info"
    def start_requests(self):
        file = open('/home/duy/code/tim kiem trinh dien/crawl/vdv.json', 'r', encoding='utf-8-sig')
        data = json.load(file)
        for i in data:
            url = i['link']
            yield scrapy.Request(url=url, callback=self.parse)
        # for url in urls:
        #     yield scrapy.Request(url=url, callback=self.parse)
    def parse(self, response):
        name = response.xpath('/html/body/div/div/div[1]/div/div[2]/div/text()').extract_first()
        country = response.xpath('/html/body/div/div/div[1]/div/div[3]/dl/dd[3]/text()').extract_first()
        dob = response.xpath('/html/body/div/div/div[1]/div/div[3]/dl/dd[5]/text()').extract_first()
        gender = response.xpath('/html/body/div/div/div[1]/div/div[3]/dl/dd[7]/text()').extract_first()
        weight = response.xpath('/html/body/div/div/div[1]/div/div[3]/dl/dd[9]/text()').extract_first()
        height = response.xpath('/html/body/div/div/div[1]/div/div[3]/dl/dd[11]/text()').extract_first()
        sport = response.xpath('//*[@id="tab-1"]/div[1]/div/div/table/tbody/tr/td[2]/text()').extract_first()
        item = CrawlerItem()
        item['name']=name.strip()
        item['country']=country.strip()
        item['dob']=dob.strip().split('(')[0].strip()
        item['gender']=gender.strip()
        item['weight']=weight.strip()[:-1].strip()
        item['height']=height.strip()
        item['sport'] = sport.strip()
        yield item
