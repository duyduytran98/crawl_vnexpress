from scrapy import Spider
from scrapy.selector import Selector
import scrapy
import sys
sys.path.append('/home/duy/code/tim kiem trinh dien/crawl/crawl')
from items import CrawlerItem
import json
import datetime

domain1 = ['thoi su', 'the gioi', 'phap luat', 'giao duc', 'khoa hoc']
domain2 = ['kinh doanh','giai tri', 'the thao', 'suc khoe', 'doi song','du lich', 'so hoa']


class CrawlerSpider(Spider):
    name = "vnexpress"
    def start_requests(self):
        urls = []
        for i in domain1:
            link1 = '-'.join(i.strip().split(' '))
            for j in range(2,100):
                url1 = 'https://vnexpress.net/{}-p{}'.format(link1,str(j))
                urls.append(url1)
        for i in domain2:
            link2 = '-'.join(i.strip().split(' '))
            for j in range(2,100):
                url2 = 'https://vnexpress.net/{}/p{}'.format(link2,str(j))
                urls.append(url2)
        # for j in range(2, 3):
        #     url = 'https://vnexpress.net/{}/p{}'.format('so-hoa', str(j))
        #     urls.append(url)
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)
    def parse(self, response):
        # thoisu.txt sua thanh {tentheloai}.txt
        article = Selector(response).xpath('/html/body/section[2]/section[1]/article')



        for dmn in domain1:
            # link1 = '-'.join(i.strip().split('-'))
            file_name = 'link/'+''.join(dmn.strip().split(' '))+'.txt'
            domain_name = 'https://vnexpress.net/'+'-'.join(dmn.strip().split(' '))+'/'
            print(file_name)
            print(domain_name)
            file = open(file_name,'a',encoding='utf-8-sig')
            for i in article:
                a = i.xpath('h4/a/@href').extract_first()
                if domain_name in a:
                    file.write(a)
                    file.write('\n')
            file.close()


        for dmn in domain2:
            # link1 = '-'.join(i.strip().split('-'))
            file_name = 'link/'+''.join(dmn.strip().split(' '))+'.txt'
            domain_name = 'https://vnexpress.net/'+'-'.join(dmn.strip().split(' '))+'/'
            print(file_name)
            print(domain_name)
            file = open(file_name,'r',encoding='utf-8-sig')
            for i in article:
                a = i.xpath('h4/a/@href').extract_first()
                if domain_name in a:
                    file.write(a)
                    file.write('\n')
            file.close()

